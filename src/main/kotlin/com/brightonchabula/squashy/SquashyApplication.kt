package com.brightonchabula.squashy

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SquashyApplication

fun main(args: Array<String>) {
    runApplication<SquashyApplication>(*args)
}
