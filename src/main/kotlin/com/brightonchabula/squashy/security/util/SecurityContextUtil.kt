package com.brightonchabula.squashy.security.util

import com.brightonchabula.squashy.security.authentication.model.CustomUserDetails
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import javax.servlet.http.HttpServletRequest

object SecurityContextUtil {

    @Throws(AccessDeniedException::class)
    fun getUsername(): String = SecurityContextHolder.getContext().authentication.name

    @Throws(AccessDeniedException::class)
    fun getCurrentUser(): CustomUserDetails? {
        val authentication = SecurityContextHolder.getContext().authentication ?: return null
        return authentication.principal as CustomUserDetails
    }

    @Throws(AccessDeniedException::class)
    fun getAuthentication(): Authentication? = authentication()


    @Throws(AccessDeniedException::class)
    fun setCurrentUser(customUserDetails: CustomUserDetails, request: HttpServletRequest? = null) {
        val authenticationToken =
            UsernamePasswordAuthenticationToken(
                customUserDetails.username,
                customUserDetails.password,
                customUserDetails.authorities
            )
        if (request != null)
            authenticationToken.details = WebAuthenticationDetailsSource().buildDetails(request)
        SecurityContextHolder.getContext().authentication = authenticationToken
    }

    @Throws(AccessDeniedException::class)
    fun getAuthorities(): MutableCollection<out GrantedAuthority> =
        if (isAuthenticated()) authentication().authorities
        else throw AccessDeniedException("Unauthorized operation. Authentication is required")

    private fun isAuthenticated(): Boolean {
        val authentication = authentication()
        return if (authentication !is AnonymousAuthenticationToken) authentication.isAuthenticated else false
    }

    private fun authentication() = SecurityContextHolder.getContext().authentication
}