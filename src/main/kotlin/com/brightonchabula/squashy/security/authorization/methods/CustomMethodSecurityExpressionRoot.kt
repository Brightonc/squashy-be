package com.brightonchabula.squashy.security.authorization.methods

import org.springframework.security.access.expression.SecurityExpressionRoot
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations
import org.springframework.security.core.Authentication

class CustomMethodSecurityExpressionRoot(authentication: Authentication) :
    SecurityExpressionRoot(authentication),
    MethodSecurityExpressionOperations {

    override fun setReturnObject(returnObject: Any?) {}

    override fun getFilterObject(): Any = false

    override fun setFilterObject(filterObject: Any?) {}

    override fun getReturnObject(): Any = false

    override fun getThis(): Any = false

    // Declare all the methods here for preAuthorise and postAuthorize


}