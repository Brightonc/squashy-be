package com.brightonchabula.squashy.security.authorization.evaluator

import com.brightonchabula.squashy.security.role.RolePermission
import com.brightonchabula.squashy.security.util.SecurityContextUtil
import org.springframework.security.access.PermissionEvaluator
import org.springframework.security.core.Authentication
import java.io.Serializable

class CustomPermissionEvaluator : PermissionEvaluator {

    override fun hasPermission(auth: Authentication?, targetObject: Any?, operation: Any?): Boolean {

        if (auth == null || targetObject == null || operation !is String) {
            return false
        }

        val resource = targetObject::class.simpleName ?: return false

        return hasPrivilege(operation, resource)
    }

    override fun hasPermission(
        auth: Authentication?,
        targetId: Serializable?,
        targetObject: String?,
        operation: Any?
    ): Boolean {

        if (auth == null || targetId == null || targetObject == null || operation !is String) {
            return false
        }

        return hasPrivilege(operation, targetObject)
    }

    private fun hasPrivilege(operation: String, resource: String): Boolean {
        return SecurityContextUtil.getAuthorities().any { authority ->
            if (authority is RolePermission)
                authority.roleOperation.name == operation && authority.roleResource.name == resource
            else false
        }
    }
}