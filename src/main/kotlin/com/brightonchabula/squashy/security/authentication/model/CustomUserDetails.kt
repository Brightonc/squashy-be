package com.brightonchabula.squashy.security.authentication.model

import com.brightonchabula.squashy.core.user.UserInfo
import com.brightonchabula.squashy.security.role.RolePermission
import com.brightonchabula.squashy.security.role.UserRole
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails

class CustomUserDetails(
    val id: Long,
    username: String,
    password: String,
    enabled: Boolean,
    accountExpired: Boolean,
    credentialsNonExpired: Boolean,
    accountNonLocked: Boolean,
    grantedAuthorities: List<GrantedAuthority>
) : User(username, password, enabled, accountExpired, credentialsNonExpired, accountNonLocked, grantedAuthorities),
    UserDetails {

    constructor(userInfo: UserInfo) : this(
        userInfo.id,
        userInfo.username,
        userInfo.password,
        !userInfo.deleted,
        !userInfo.deleted,
        !userInfo.deleted,
        !userInfo.deleted,
        buildAuthorities(userInfo.userRoles, userInfo.rolePermissions)
    )

    companion object {
        private fun buildAuthorities(userRoles: Set<UserRole>, rolePermissions: Set<RolePermission>)
                : List<GrantedAuthority> = listOf(*userRoles.toTypedArray(), *rolePermissions.toTypedArray())
    }
}