package com.brightonchabula.squashy.security.authentication

import com.brightonchabula.squashy.security.scrambler.ScramblingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class CustomAuthenticationProvider : DaoAuthenticationProvider() {

    @Autowired
    lateinit var scramblingService: ScramblingService
    @Autowired
    lateinit var customUserDetailsService: CustomUserDetailsService

    @PostConstruct
    fun init() {
        this.passwordEncoder = scramblingService.getEncoder()
        this.userDetailsService = customUserDetailsService
    }
}