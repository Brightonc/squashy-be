package com.brightonchabula.squashy.security.authentication.filter

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.io.IOUtils
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.http.HttpServletRequest

class CustomAuthenticationFilter : UsernamePasswordAuthenticationFilter() {

    var loginRequest = LoginRequest()

    override fun obtainUsername(request: HttpServletRequest?): String? {
        if (!loginRequest.isAvailable()) setRequestBody(request)
        return loginRequest.username
    }

    override fun obtainPassword(request: HttpServletRequest?): String? {
        if (!loginRequest.isAvailable()) setRequestBody(request)
        return loginRequest.password
    }

    private fun setRequestBody(request: HttpServletRequest?) {
        val requestBody = IOUtils.toString(request?.reader)
        loginRequest = ObjectMapper().readValue(requestBody, LoginRequest::class.java)
    }

}

class LoginRequest{
    private var usernameAvailable = false
    var username: String = ""
        get(): String {
            if (usernameAvailable) {
                usernameAvailable = false
                return field
            }

            return ""
        }
        set(value) {
            field = value
            usernameAvailable = true
        }

    private var passwordAvailable = false
    var password: String = ""
        get(): String {
            if (passwordAvailable) {
                passwordAvailable = false
                return field
            }

            return ""
        }
        set(value) {
            field = value
            passwordAvailable = true
        }

    fun isAvailable() = usernameAvailable || passwordAvailable
}