package com.brightonchabula.squashy.security.authentication

import com.brightonchabula.squashy.core.user.UserInfo
import com.brightonchabula.squashy.core.user.UserInfoService
import com.brightonchabula.squashy.security.authentication.model.CustomUserDetails
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserDetailsService (@Autowired private val userInfoService: UserInfoService) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String?): CustomUserDetails {
        val userInfo = userInfoService.fetchByUserName(username)
        return CustomUserDetails(UserInfo(userInfo))
    }
}