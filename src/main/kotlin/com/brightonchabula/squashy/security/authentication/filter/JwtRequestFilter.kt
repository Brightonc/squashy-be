package com.brightonchabula.squashy.security.authentication.filter

import com.brightonchabula.squashy.security.authentication.CustomUserDetailsService
import com.brightonchabula.squashy.security.token.JwtService
import com.brightonchabula.squashy.security.token.TokenJwt
import com.brightonchabula.squashy.security.util.SecurityContextUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtRequestFilter @Autowired constructor(
    private val jwtService: JwtService,
    private val userDetailsService: CustomUserDetailsService
) : OncePerRequestFilter() {

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {

        val authorizationHeader = request.getHeader("Authorization")
        var username: String? = null
        var tokenJwt: TokenJwt? = null

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            tokenJwt = TokenJwt(authorizationHeader.substring(7))
            username = jwtService.extractUsername(tokenJwt)
        }

        if (username != null && SecurityContextUtil.getAuthentication() == null) {
            val userDetails = userDetailsService.loadUserByUsername(username)

            if(jwtService.validateToken(tokenJwt!!, userDetails)) {
                SecurityContextUtil.setCurrentUser(userDetails)
            }
        }

        chain.doFilter(request, response)
    }
}