package com.brightonchabula.squashy.security.role

import com.brightonchabula.squashy.core.base.BaseDto
import com.brightonchabula.squashy.core.base.BaseEntity
import com.brightonchabula.squashy.core.base.BaseRepository
import org.hibernate.annotations.SQLDelete
import javax.persistence.Entity

data class RoleOperationDto(
    val name: String = "",
    val description: String = ""
) : BaseDto() {

    constructor(id: Long, name: String, description: String) : this(name, description) {
        this.id = id
    }
}

@Entity
@SQLDelete(sql = "update role_operation set deleted = false")
data class RoleOperation(
    val name: String = "",
    val description: String = ""
) : BaseEntity<RoleOperationDto>() {
    constructor(dto: RoleOperationDto) : this(dto.name, dto.description) {
        this.id = dto.id
    }

    override fun toDto() = RoleOperationDto(id, name, description)
}

interface RoleOperationRepository: BaseRepository<RoleOperation>