package com.brightonchabula.squashy.security.role

import com.brightonchabula.squashy.core.base.BaseDto
import com.brightonchabula.squashy.core.base.BaseEntity
import com.brightonchabula.squashy.core.base.BaseRepository
import org.hibernate.annotations.SQLDelete
import javax.persistence.Entity

data class RoleResourceDto(
    val name: String = ""
) : BaseDto() {

    constructor(id: Long, name: String) : this(name) {
        this.id = id
    }
}

@Entity
@SQLDelete(sql = "update role_resource set deleted = false")
data class RoleResource(
    val name: String = ""
) : BaseEntity<RoleResourceDto>() {

    constructor(dto: RoleResourceDto) : this(dto.name) {
        this.id = dto.id
    }

    override fun toDto() = RoleResourceDto(id, name)
}


interface RoleResourceRepository : BaseRepository<RoleResource>