package com.brightonchabula.squashy.security.role

import com.brightonchabula.squashy.core.base.BaseDto
import com.brightonchabula.squashy.core.base.BaseEntity
import com.brightonchabula.squashy.core.base.BaseRepository
import org.hibernate.annotations.SQLDelete
import org.springframework.security.core.GrantedAuthority
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

data class RolePermissionDto(
    val name: String = "",
    val roleOperation: RoleOperationDto = RoleOperationDto(),
    val roleResource: RoleResourceDto = RoleResourceDto()
) : BaseDto() {

    constructor(id: Long, name: String, roleOperation: RoleOperationDto, roleResource: RoleResourceDto)
    : this (name, roleOperation, roleResource) {
        this.id = id
    }
}

@Entity
@SQLDelete(sql = "update role_permission set deleted = false")
data class RolePermission(
    val name: String = "",
    @ManyToOne
    @JoinColumn(name = "role_operation_id")
    val roleOperation: RoleOperation = RoleOperation(),
    @ManyToOne
    @JoinColumn(name = "role_resource_id")
    val roleResource: RoleResource = RoleResource()
) : BaseEntity<RolePermissionDto>(), GrantedAuthority {

    constructor(dto: RolePermissionDto) : this(dto.name, RoleOperation(dto.roleOperation), RoleResource(dto.roleResource)) {
        this.id = dto.id
    }

    override fun toDto() = RolePermissionDto(id, name, roleOperation.toDto(), roleResource.toDto())

    override fun getAuthority(): String = name
}

interface RolePermissionRepository : BaseRepository<RolePermission>