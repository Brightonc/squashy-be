package com.brightonchabula.squashy.security.role

import com.brightonchabula.squashy.core.base.BaseDto
import com.brightonchabula.squashy.core.base.BaseEntity
import com.brightonchabula.squashy.core.base.BaseRepository
import org.hibernate.annotations.SQLDelete
import org.springframework.security.core.GrantedAuthority
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany

data class UserRoleDto(
    val name: String = "",
    val description: String = "",
    val rolePermissions: Set<RolePermissionDto> = setOf()
) : BaseDto() {

    constructor(id: Long, name: String, description: String, rolePermissions: Set<RolePermissionDto>)
    : this(name, description, rolePermissions) {
        this.id = id
    }
}

@Entity
@SQLDelete(sql = "update user_role set deleted = false")
data class UserRole(
    val name: String = "",
    val description: String = "",
    @ManyToMany
    @JoinTable(
        name = "user_roles_role_permissions",
        joinColumns = [JoinColumn(name = "user_role_id", referencedColumnName = "id") ],
        inverseJoinColumns = [JoinColumn(name = "role_permission_id", referencedColumnName = "id")]
    )
    val rolePermissions: Set<RolePermission> = setOf()
) : BaseEntity<UserRoleDto>(), GrantedAuthority {

    constructor(dto: UserRoleDto) :
            this(
                dto.name,
                dto.description,
                dto.rolePermissions.map { RolePermission(it) }.toSet()
            )

    override fun toDto() = UserRoleDto(id, name, description, rolePermissions.map { it.toDto() }.toSet())

    override fun getAuthority(): String = name
}

enum class UserRoleType {
    ROLE_SUPER_USER,
    ROLE_USER
}

interface UserRoleRepository : BaseRepository<UserRole>