package com.brightonchabula.squashy.security.token

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.Date

data class TokenJwt(
    val token: String
)

data class TokenClaims(
    val uid: Long,
    val iat: Date,
    val exp: Date,
    val sub: String,
    val su: Boolean = false
) {
    fun toMap(): Map<String, Any> = ObjectMapper().convertValue(this, object : TypeReference<Map<String, Any>>() {})
}
