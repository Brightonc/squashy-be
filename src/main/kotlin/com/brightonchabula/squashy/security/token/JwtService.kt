package com.brightonchabula.squashy.security.token

import com.brightonchabula.squashy.security.authentication.model.CustomUserDetails
import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.impl.TextCodec
import mu.KLogging
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.stereotype.Service
import java.util.Date
import kotlin.reflect.KFunction1

interface JwtService {
    fun generateToken(userDetails: CustomUserDetails): TokenJwt
    fun validateToken(tokenJwt: TokenJwt, userDetails: CustomUserDetails): Boolean
    fun extractUsername(tokenJwt: TokenJwt): String
}


@Service
class JwtServiceImpl : JwtService {

    companion object : KLogging()

    // todo put a more appropriate security key
    private val SECRET_KEY = "secret"

    override fun generateToken(userDetails: CustomUserDetails): TokenJwt {
        val claims = TokenClaims(
            uid = userDetails.id,
            iat = Date(System.currentTimeMillis()),
            exp = Date(System.currentTimeMillis() * 1000 * 60 * 60 * 24 * 7),
            sub = userDetails.username
        )

        return createToken(claims, userDetails.username)
    }

    override fun validateToken(tokenJwt: TokenJwt, userDetails: CustomUserDetails): Boolean {
        val username = extractUsername(tokenJwt)
        return (username == userDetails.username && !isTokenExpired(tokenJwt))
    }

    override fun extractUsername(tokenJwt: TokenJwt): String {
        return extractClaims(tokenJwt, Claims::getSubject)
    }

    private fun extractExpiration(tokenJwt: TokenJwt): Date {
        return extractClaims(tokenJwt, Claims::getExpiration)
    }

    private fun <T> extractClaims(tokenJwt: TokenJwt, claimsResolver: KFunction1<Claims, T>): T {
        val claims = extractAllClaims(tokenJwt)
        return claimsResolver.invoke(claims)
    }

    private fun extractAllClaims(tokenJwt: TokenJwt): Claims {
        try {
            return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(tokenJwt.token).body
        } catch (ex: Exception) {
            logger.error { ex.localizedMessage }
            throw BadCredentialsException("Bad credentials")
        }
    }

    private fun isTokenExpired(tokenJwt: TokenJwt): Boolean {
        return extractExpiration(tokenJwt).before(Date())
    }

    private fun createToken(claims: TokenClaims, subject: String): TokenJwt {
        return TokenJwt(Jwts.builder()
            .setClaims(claims.toMap())
            .setSubject(subject)
            .setIssuedAt(claims.iat)
            .setExpiration(claims.exp)
            .signWith(
                SignatureAlgorithm.HS256,
                TextCodec.BASE64.decode(SECRET_KEY)
            ).compact())
    }

}