package com.brightonchabula.squashy.security.scrambler

import org.springframework.security.crypto.password.PasswordEncoder

interface ScramblingService {

    fun getEncoder(): PasswordEncoder
    fun encode(pass: String): String
    fun matches(enteredPass: String, storedPass: String): Boolean
}