package com.brightonchabula.squashy.security.scrambler

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class ScramblingServiceImpl : ScramblingService {

    //todo work on encryption salting

    private val encoder: PasswordEncoder = BCryptPasswordEncoder()

    override fun matches(enteredPass: String, storedPass: String) = encoder.matches(enteredPass, storedPass)
    override fun encode(pass: String): String = encoder.encode(pass)
    override fun getEncoder(): PasswordEncoder = encoder
}