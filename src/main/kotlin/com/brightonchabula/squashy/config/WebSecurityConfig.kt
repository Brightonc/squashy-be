package com.brightonchabula.squashy.config

import com.brightonchabula.squashy.core.base.BASE_API_PREFIX
import com.brightonchabula.squashy.security.authentication.CustomAuthenticationProvider
import com.brightonchabula.squashy.security.authentication.filter.CustomAuthenticationFilter
import com.brightonchabula.squashy.security.authentication.filter.JwtRequestFilter
import com.brightonchabula.squashy.security.token.JwtService
import com.brightonchabula.squashy.security.util.SecurityContextUtil
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.lang.Exception
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Configuration
@EnableWebSecurity
class WebSecurityConfig @Autowired constructor(
    private val jwtRequestFilter: JwtRequestFilter,
    private val customAuthenticationProvider: CustomAuthenticationProvider,
    private val jwtService: JwtService
) : WebSecurityConfigurerAdapter() {

    private val objectMapper by lazy { ObjectMapper() }

    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth!!.authenticationProvider(customAuthenticationProvider)
    }

    override fun configure(http: HttpSecurity?) {
        http!!.authorizeRequests()
            .antMatchers(HttpMethod.GET, "$BASE_API_PREFIX/secure").authenticated()
            .antMatchers(HttpMethod.GET, "$BASE_API_PREFIX/**").permitAll()
            .antMatchers(HttpMethod.POST, "$BASE_API_PREFIX/**").permitAll()
            .antMatchers(HttpMethod.PUT, "$BASE_API_PREFIX/**").permitAll()
            .antMatchers(HttpMethod.DELETE, "$BASE_API_PREFIX/**").permitAll()
            .anyRequest().authenticated()
            .and()
            .addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)
            .addFilterBefore(authenticationFilter(), CustomAuthenticationFilter::class.java)
            .logout()
            .logoutUrl("$BASE_API_PREFIX/logout").permitAll()
            .logoutSuccessUrl("/logout")
            .and()
            .csrf().disable()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    @Bean
    @Throws(Exception::class)
    fun authenticationFilter(): CustomAuthenticationFilter {
        val authFilter = CustomAuthenticationFilter()
        authFilter.setAuthenticationManager(authenticationManager())
        authFilter.setFilterProcessesUrl("$BASE_API_PREFIX/login")
        authFilter.setAuthenticationSuccessHandler(this::handleLoginSuccessful)
        authFilter.setAuthenticationFailureHandler(this::handleLoginFailure)
        return authFilter
    }

    fun handleLoginSuccessful(
        request: HttpServletRequest,
        response: HttpServletResponse,
        auth: Authentication) {

        val userDetails = SecurityContextUtil.getCurrentUser()
        val token = jwtService.generateToken(userDetails!!)
        val writer = response.writer
        objectMapper.writeValue(writer, token)
        writer.flush()
    }

    fun handleLoginFailure(
        request: HttpServletRequest,
        response: HttpServletResponse,
        authException: AuthenticationException) {

        response.status = HttpStatus.UNAUTHORIZED.value()
        response.writer.print(authException.localizedMessage)
    }
}