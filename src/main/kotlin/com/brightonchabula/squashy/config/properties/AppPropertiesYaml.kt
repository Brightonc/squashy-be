package com.brightonchabula.squashy.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

@EnableConfigurationProperties
@ConfigurationProperties(prefix = "app")
@Configuration
class ApplicationPropertiesCore {
}