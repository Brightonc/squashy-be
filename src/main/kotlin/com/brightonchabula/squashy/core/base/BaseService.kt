package com.brightonchabula.squashy.core.base

interface BaseService<T : BaseDto> {
    fun fetchAll(
        criteria: String? = null,
        page: Int? = null,
        size: Int? = null,
        sortProperty: String? = null
    ): List<T>
    fun fetchById(id: Long): T
    fun fetchByIds(ids: Set<Long>): List<T>
    fun save(t: T): T
    fun save(t: Collection<T>): Collection<T>
    fun update(t: T): T
    fun delete(id: Long): Boolean
    fun delete(ids: Set<Long>): Boolean
}