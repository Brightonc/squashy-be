package com.brightonchabula.squashy.core.base

import org.hibernate.annotations.GenericGenerator
import java.io.Serializable
import java.sql.Timestamp
import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class BaseEntity<out T: BaseDto>(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    var id: Long = 0,
    @Column(insertable = false, updatable = false)
    var createdOn: Timestamp? = null,
    @Column(insertable = false, updatable = false)
    var modifiedOn: Timestamp? = null,
    @Column(insertable = false, updatable = false)
    var createdBy: Long = 0,
    @Column(insertable = false, updatable = false)
    var modifiedBy: Long = 0,
    @Column(insertable = false, updatable = false)
    var deleted: Boolean = false
) : Serializable {

    abstract fun toDto(): T
}