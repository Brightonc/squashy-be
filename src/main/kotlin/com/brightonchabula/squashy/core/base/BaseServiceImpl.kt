package com.brightonchabula.squashy.core.base

import javassist.NotFoundException
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.transaction.annotation.Transactional
import java.lang.IllegalArgumentException

@Transactional
abstract class BaseServiceImpl<E : BaseEntity<T>, T : BaseDto>(
    private val repository: BaseRepository<E>,
    private val entityClazz: Class<E>
) : BaseService<T> {
    @Throws(NotFoundException::class)
    override fun fetchAll(
        criteria: String?,
        page: Int?,
        size: Int?,
        sortProperty: String?
    ): List<T> = repository.findAllByDeletedFalse(createPageRequest(page, size, sortProperty)).map { it.toDto() }

    @Throws(NotFoundException::class)
    override fun fetchById(id: Long): T = findEntityById(id).toDto()

    @Throws(NotFoundException::class)
    override fun fetchByIds(ids: Set<Long>): List<T> = repository.findByIdInAndDeletedFalse(ids).map { it.toDto() }

    @Throws(IllegalArgumentException::class)
    override fun save(t: T): T {
        val entity = entityClazz.getConstructor(t.javaClass).newInstance(t) as E
        return repository.save(entity).toDto()
    }

    @Throws(IllegalArgumentException::class)
    override fun save(t: Collection<T>): Collection<T> {
        val constructor = entityClazz.getConstructor(t.first().javaClass)
        val entities = t.map { constructor.newInstance(it) }
        return repository.saveAll(entities).map { it.toDto() }
    }

    @Throws(IllegalArgumentException::class)
    override fun update(t: T): T =
        if (repository.existsById(t.id)) save(t)
        else throw entityNotFoundException(t.id)


    @Throws(NotFoundException::class)
    override fun delete(id: Long): Boolean {
        repository.deleteById(id)
        return true
    }

    @Throws(NotFoundException::class)
    override fun delete(ids: Set<Long>): Boolean {
        ids.map {delete(it) }
        return true
    }

    private fun findEntityById(id: Long) = repository.findByIdAndDeletedFalse(id)
        ?: throw entityNotFoundException(id)

    private fun createPageRequest(page: Int?, size: Int?, sortProperty: String?) =
        if (page == null && size == null) null
    else PageRequest.of(
            page!!,
            size!!,
            Sort.by(Sort.Direction.DESC, if (sortProperty.isNullOrBlank()) "id" else sortProperty)
        )

    private fun entityNotFoundException(id: Long) = NotFoundException("No ${entityClazz.simpleName} found with id $id")
}