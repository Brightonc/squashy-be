package com.brightonchabula.squashy.core.base

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface BaseRepository<E: BaseEntity<*>> : JpaRepository<E, Long> {
    fun findAllByDeletedFalse(pageable: Pageable? = null): List<E>
    fun findByIdAndDeletedFalse(id: Long): E?
    fun findByIdInAndDeletedFalse(ids: Set<Long>): List<E>
}