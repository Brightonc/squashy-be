package com.brightonchabula.squashy.core.base

import java.io.Serializable

abstract class BaseDto(var id: Long = 0) : Serializable