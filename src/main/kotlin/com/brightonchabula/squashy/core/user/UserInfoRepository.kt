package com.brightonchabula.squashy.core.user

import com.brightonchabula.squashy.core.base.BaseRepository

interface UserInfoRepository : BaseRepository<UserInfo> {
    fun findByUsernameAndDeletedFalse(username: String): UserInfo?
    fun findByEmailAndDeletedFalse(email: String): UserInfo?
}