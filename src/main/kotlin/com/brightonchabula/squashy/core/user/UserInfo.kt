package com.brightonchabula.squashy.core.user

import com.brightonchabula.squashy.core.base.BaseDto
import com.brightonchabula.squashy.core.base.BaseEntity
import com.brightonchabula.squashy.security.role.RolePermission
import com.brightonchabula.squashy.security.role.RolePermissionDto
import com.brightonchabula.squashy.security.role.UserRole
import com.brightonchabula.squashy.security.role.UserRoleDto
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.annotations.SQLDelete
import java.sql.Date
import java.sql.Timestamp
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany

@Entity
@SQLDelete(sql = "update user_info set delete = false")
data class UserInfo(
    val username: String = "",
    val firstName: String = "",
    val lastName: String = "",
    val email: String = "",
    @Column(insertable = false, updatable = false)
    val password: String = "",
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_infos_user_roles",
        joinColumns = [JoinColumn(name = "user_info_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "user_role_id", referencedColumnName = "id")]
    )
    val userRoles: Set<UserRole> = emptySet(),
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "user_infos_role_permissions",
        joinColumns = [JoinColumn(name = "user_info_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "role_permission_id", referencedColumnName = "id")]
    )
    val rolePermissions: Set<RolePermission> = emptySet(),
    val dob: Date? = null,
    @Column(insertable = false, updatable = false)
    val loginCount: Int = 0,
    @Column(insertable = false, updatable = false)
    val lastLogin: Timestamp? = null,
    @Column(insertable = false, updatable = false)
    val credentialsExpired: Boolean = false
) : BaseEntity<UserInfoDto>() {

    constructor(dto: UserInfoDto) : this(
        dto.username,
        dto.firstName,
        dto.lastName,
        dto.email,
        dto.password,
        dto._userRoles.map { UserRole(it) }.toSet(),
        dto._rolePermissions.map { RolePermission(it) }.toSet(),
        dto.dob,
        dto.loginCount,
        dto.lastLogin,
        dto.credentialsExpired
    ) {
        this.id = dto.id
    }

    override fun toDto(): UserInfoDto =
        UserInfoDto(
            id,
            username,
            firstName,
            lastName,
            email,
            password,
            userRoles.map { it.toDto() },
            rolePermissions.map { it.toDto() },
            dob,
            loginCount,
            lastLogin,
            credentialsExpired
        )
}

data class UserInfoDto(
    val username: String = "",
    val firstName: String = "",
    val lastName: String = "",
    val email: String = "",
    @get:JsonIgnore
    @set:JsonProperty
    var password: String = "",
    @JsonIgnore
    val _userRoles: List<UserRoleDto> = emptyList(),
    @JsonIgnore
    val _rolePermissions: List<RolePermissionDto> = emptyList(),
    val dob: Date? = null,
    val loginCount: Int = 0,
    @JsonIgnore
    val lastLogin: Timestamp? = null,
    val credentialsExpired: Boolean = false
) : BaseDto() {
    constructor(
        id: Long,
        username: String,
        firstName: String,
        lastName: String,
        email: String,
        password: String,
        userRoles: List<UserRoleDto>,
        rolePermissions: List<RolePermissionDto>,
        dob: Date?,
        loginCount: Int,
        lastLogin: Timestamp?,
        credentialsExpired: Boolean
    ) : this(
        username,
        firstName,
        lastName,
        email,
        password,
        userRoles,
        rolePermissions,
        dob,
        loginCount,
        lastLogin,
        credentialsExpired
    ) {
        this.id = id
    }
}