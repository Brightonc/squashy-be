package com.brightonchabula.squashy.core.user

import com.brightonchabula.squashy.core.base.BaseService
import com.brightonchabula.squashy.core.base.BaseServiceImpl
import com.brightonchabula.squashy.security.scrambler.ScramblingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

interface UserInfoService : BaseService<UserInfoDto> {
    fun fetchByUserName(username: String?): UserInfoDto
}

@Service
class UserInfoServiceImpl @Autowired constructor(
    private val userInfoRepo: UserInfoRepository,
    private val scramblingService: ScramblingService
) : BaseServiceImpl<UserInfo, UserInfoDto>(userInfoRepo, UserInfo::class.java), UserInfoService {

    // Todo need to be aware of the problem the changing of username can bring in the future,
    // todo need to track every username created
    // todo can change a username anytime,
    // todo validate username with the backend from the frontend

    override fun fetchByUserName(username: String?): UserInfoDto {
        if (username.isNullOrBlank())
            throw UsernameNotFoundException("could not find user info with username: $username")
        return userInfoRepo.findByEmailAndDeletedFalse(username)?.toDto()
            ?: userInfoRepo.findByUsernameAndDeletedFalse(username)?.toDto()
            ?: throw UsernameNotFoundException("could not find user info with username: $username")
    }


    override fun save(userInfo: UserInfoDto): UserInfoDto {
        userInfo.password = scramblingService.encode(userInfo.password)
        val savedUser = userInfoRepo.save(UserInfo(userInfo))
        return savedUser.toDto()
    }
}