package com.brightonchabula.squashy.controllers

import com.brightonchabula.squashy.core.base.BASE_API_PREFIX
import com.brightonchabula.squashy.core.user.UserInfoDto
import com.brightonchabula.squashy.core.user.UserInfoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(BASE_API_PREFIX)
class UserInfoController @Autowired constructor(
    private val userInfoService: UserInfoService
) {

    @PostMapping("/signup")
    fun save(@RequestBody userInfo: UserInfoDto) = userInfoService.save(userInfo)

    @GetMapping("users/{id}")
    fun fetchById(@PathVariable id: Long) = userInfoService.fetchById(id)
}