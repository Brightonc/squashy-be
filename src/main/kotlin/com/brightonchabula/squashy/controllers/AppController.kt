package com.brightonchabula.squashy.controllers

import com.brightonchabula.squashy.core.base.BASE_API_PREFIX
import com.brightonchabula.squashy.security.util.SecurityContextUtil
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
class AppController {

    @GetMapping("$BASE_API_PREFIX/")
    fun getHome() = "Hello " + SecurityContextUtil.getUsername()

    @GetMapping("$BASE_API_PREFIX/secure")
    fun getSecureHome() = "Hello " + SecurityContextUtil.getUsername()

    @RequestMapping("$BASE_API_PREFIX/login")
    fun getLogin() = "Login here " + SecurityContextUtil.getUsername()

    @PostMapping("$BASE_API_PREFIX/login")
    fun saveLogin() = "Login here " + SecurityContextUtil.getUsername()

    @GetMapping("/logout")
    fun logout(): ResponseEntity<Any> = ResponseEntity.ok("logged out successfully")

}