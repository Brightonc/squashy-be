# --brighton - init core tables

create table if not exists user_info
(
    id                 bigint     not null auto_increment,
    username           varchar(64),
    first_name         varchar(64),
    last_name          varchar(64),
    email              varchar(64),
    password           varchar(255),
    dob                date,
    login_count        int(11)    not null default 0,
    last_login         timestamp           default null,
    credentials_expired tinyint(1) not null default 0,
    created_on         timestamp  not null default now(),
    modified_on        timestamp  not null default now() on update now(),
    created_by         bigint     not null default 0,
    modified_by        bigint     not null default 0,
    deleted            tinyint(1) not null default 0,
    primary key (id)
);

create table if not exists role_resource
(
    id          bigint      not null auto_increment,
    name        varchar(32) not null,
    created_on  timestamp   not null default now(),
    modified_on timestamp   not null default now() on update now(),
    created_by  bigint      not null default 0,
    modified_by bigint      not null default 0,
    deleted     tinyint(1)  not null default 0,
    primary key (id)
);

create table if not exists role_operation
(
    id          bigint      not null auto_increment,
    name        varchar(32) not null,
    description varchar(32) not null,
    created_on  timestamp   not null default now(),
    modified_on timestamp   not null default now() on update now(),
    created_by  bigint      not null default 0,
    modified_by bigint      not null default 0,
    deleted     tinyint(1)  not null default 0,
    primary key (id)
);


create table if not exists role_permission
(
    id                bigint      not null auto_increment,
    name              varchar(32) not null,
    role_operation_id bigint,
    role_resource_id  bigint,
    created_on        timestamp   not null default now(),
    modified_on       timestamp   not null default now() on update now(),
    created_by        bigint      not null default 0,
    modified_by       bigint      not null default 0,
    deleted           tinyint(1)  not null default 0,
    primary key (id),
    foreign key PK_user_permission_role_operation_id (role_operation_id) references role_operation (id),
    foreign key PK_user_permission_role_resource_id (role_resource_id) references role_resource (id)
);

create table if not exists user_role
(
    id          bigint      not null auto_increment,
    name        varchar(32) not null,
    description varchar(32) not null,
    created_on  timestamp   not null default now(),
    modified_on timestamp   not null default now() on update now(),
    created_by  bigint      not null default 0,
    modified_by bigint      not null default 0,
    deleted     tinyint(1)  not null default 0,
    primary key (id)
);

create table if not exists user_roles_role_permissions
(
    id                 bigint not null auto_increment,
    user_role_id       bigint,
    role_permission_id bigint,
    primary key (id),
    foreign key PK_user_roles_role_permissions_user_role_id (user_role_id) references user_info (id),
    foreign key PK_user_roles_role_permissions_role_permission_id (role_permission_id) references role_permission (id)
);

create table if not exists user_infos_user_roles
(
    id           bigint not null auto_increment,
    user_info_id bigint,
    user_role_id bigint,
    primary key (id),
    foreign key PK_user_info_user_info_id (user_info_id) references user_info (id),
    foreign key PK_user_info_user_role_id (user_role_id) references user_role (id)
)